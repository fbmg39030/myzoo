//
// Created by fbmg39030 on 27/03/21.
//

#ifndef MYZOO_ZOOEXCEPTION_H
#define MYZOO_ZOOEXCEPTION_H

#include <exception>
#include <stdexcept>
#include <iostream>

using namespace std;

class ZooException: public runtime_error {
private:
    //static string help;
public:

    virtual const char * what() const noexcept override{
        return "Exception: ";
    }
    using runtime_error::runtime_error;
};


#endif //MYZOO_ZOOEXCEPTION_H
