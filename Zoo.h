//
// Created by fbmg39030 on 28/03/21.
//

#ifndef MYZOO_ZOO_H
#define MYZOO_ZOO_H

#include "vector"
#include "Animals/Animal.h"
using namespace std;


class Zoo {

public:
    vector<Animal*> vecAnimals;
    int animalsID=0;
    int addAnimal(Animal* sensor);
    Animal* getAnimal(int id);
    void clearZoo();
    void deleteAnimal(int id);
    void feedingTime();
    Food * pickRandomFood();
    Animal *getHaviestHerbivore();
    Animal *getHaviestCarnivore();
};


#endif //MYZOO_ZOO_H
