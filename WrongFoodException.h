//
// Created by fbmg39030 on 27/03/21.
//

#ifndef MYZOO_WRONGFOODEXCEPTION_H
#define MYZOO_WRONGFOODEXCEPTION_H


class WrongFoodException: public ZooException {
private:
    string animal;
    string food;
public:

    const char * what() const noexcept override{
        //string ss;
        //ss << this->animal<< " doesn't like this food (" << this->food + ")";
        return "didn't like the food";
    }
    WrongFoodException() : ZooException("") {

    }
};




#endif //MYZOO_WRONGFOODEXCEPTION_H
