//
// Created by fbmg39030 on 27/03/21.
//

#ifndef MYZOO_RIGHTFOODEXCEPTION_H
#define MYZOO_RIGHTFOODEXCEPTION_H


#include "ZooException.h"

class RightFoodException: public ZooException {
private:
    string animal="";
    string food="";
public:

    const char * what() const noexcept override{
        return "liked the food";// animal + " doesn't like this food (" + food + ")";;
    }

    RightFoodException() : ZooException("") {

    }
};


#endif //MYZOO_RIGHTFOODEXCEPTION_H
