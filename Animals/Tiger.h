//
// Created by fbmg39030 on 26/03/21.
//

#ifndef MYZOO_TIGER_H
#define MYZOO_TIGER_H


#include "Carnivore.h"

class Tiger: public Carnivore {
private:
    string type="Tiger";
public:
    static int weight;
    Tiger( ) : Carnivore(weight){

    }
    virtual ~Tiger(){
        //cout<<"Destroying Tiger obj"<<endl;
    }
    string getAnimalType() override{
        return type;
    }
};


#endif //MYZOO_TIGER_H
