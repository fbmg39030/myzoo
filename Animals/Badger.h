//
// Created by fbmg39030 on 26/03/21.
//

#ifndef MYZOO_BADGER_H
#define MYZOO_BADGER_H


#include "Carnivore.h"

class Badger: public Carnivore {
private:
    string type="Badger";
public:
    static int weight;
    Badger( ):Carnivore(weight){

    }
    virtual ~Badger(){
        //cout<<"Destroying Badger obj"<<endl;
    }
    string getAnimalType() override{
        return type;
    }

};


#endif //MYZOO_BADGER_H
