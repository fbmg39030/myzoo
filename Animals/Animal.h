//
// Created by fbmg39030 on 26/03/21.
//

#ifndef MYZOO_ANIMAL_H
#define MYZOO_ANIMAL_H


#include "../Food/Food.h"

class Animal {
private:
    int weight;
    int ID;

public:
    virtual ~Animal() {
        //cout<<"Destroying Animal obj"<<endl<<endl;
    }

    Animal(int weight ){
        this->weight=weight;
    }
    virtual int getWeight()  {
        return weight;
    }

    virtual string getAnimalType()  {
        return "";
    }
    int feeding(Food* food);


    void setId(int id) {
        ID = id;
    }
    int getId() {
        return ID;
    }
};


#endif //MYZOO_ANIMAL_H
