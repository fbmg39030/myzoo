//
// Created by fbmg39030 on 26/03/21.
//

#ifndef MYZOO_HERBIVORE_H
#define MYZOO_HERBIVORE_H


#include "Animal.h"

class Herbivore: public Animal {
public:
    Herbivore(int weight ):Animal(weight){

    }

    virtual ~Herbivore() {
        //cout<<"Destroying Herbivore obj"<<endl;
    }
};


#endif //MYZOO_HERBIVORE_H
