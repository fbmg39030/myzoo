//
// Created by fbmg39030 on 26/03/21.
//

#ifndef MYZOO_CARNIVORE_H
#define MYZOO_CARNIVORE_H


#include "Animal.h"

class Carnivore: public Animal {
public:
    Carnivore( int weight):Animal(weight){

    }

    virtual ~Carnivore() {
        //cout<<"Destroying Carnivore obj"<<endl;
    }

};


#endif //MYZOO_CARNIVORE_H
