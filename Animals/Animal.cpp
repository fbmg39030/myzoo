//
// Created by fbmg39030 on 26/03/21.
//

#include "Animal.h"
#include "../Food/Bambus.h"
#include "../ZooException.h"
#include "Herbivore.h"
#include "../Food/Meet.h"
#include "../RightFoodException.h"
#include "../WrongFoodException.h"

int Animal::feeding(Food *food) {
    try{
        if(this->getAnimalType()=="Badger"){
            if(food->getType()=="Meet"){
                throw RightFoodException();
            }
            else if(food->getType()=="Plant") {
                Plants* child = dynamic_cast<Plants*>(food);
                if(child!=NULL && child->getPlantType()=="Grass"){
                    throw RightFoodException();
                }else{
                    throw WrongFoodException();
                }
            }
            else{
                throw WrongFoodException();
            }
        }
        else if (this->getAnimalType()=="Tiger"){
            if(food->getType()=="Meet"){
                throw RightFoodException();
            }
            else{
                throw WrongFoodException();
            }
        }
        else if (this->getAnimalType()=="Elephant"){
            if(food->getType()=="Plant") {
                throw RightFoodException();
            }
            else{
                throw WrongFoodException();
            }
        }
        else if (this->getAnimalType()=="Panda"){

            Plants* child = dynamic_cast<Plants*>(food);
            if(child!=NULL && child->getPlantType()=="Bambus"){
                throw RightFoodException();
            }
            else{
                throw WrongFoodException();
            }
        }
    }catch (RightFoodException &exception){
        if(food->getType()=="Plant") {
            Plants *child = dynamic_cast<Plants *>(food);
            cout<<this->getAnimalType()<<" "<<exception.what()<<"("<<child->getPlantType()<<")"<<endl;
        }
        else{
            cout<<this->getAnimalType()<<" "<<exception.what()<<"("<<food->getType()<<")"<<endl;
        }
        food= nullptr;
        free(food);
        return 0;
    }
    catch (WrongFoodException &exception) {
        if(food->getType()=="Plant") {
            Plants *child = dynamic_cast<Plants *>(food);
            cout<<this->getAnimalType()<<" "<<exception.what()<<"("<<child->getPlantType()<<")"<<endl;
        }
        else{
            cout<<this->getAnimalType()<<" "<<exception.what()<<"("<<food->getType()<<")"<<endl;
        }
        food= nullptr;
        free(food);
        return 1;
    }
    return 0;
}

