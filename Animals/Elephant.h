//
// Created by fbmg39030 on 26/03/21.
//

#ifndef MYZOO_ELEPHANT_H
#define MYZOO_ELEPHANT_H


#include "Herbivore.h"

class Elephant: public Herbivore {
private:
    string type="Elephant";
public:
    static int weight;
    Elephant( ):Herbivore(weight){

    }
    ~Elephant(){
        //cout<<"Destroying Elephant obj"<<endl;
    }

    string getAnimalType() override{
        return type;
    }
};


#endif //MYZOO_ELEPHANT_H
