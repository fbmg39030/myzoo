//
// Created by fbmg39030 on 26/03/21.
//

#ifndef MYZOO_PANDA_H
#define MYZOO_PANDA_H


#include "Herbivore.h"
class Panda: public Herbivore {
private:
    string type="Panda";
public:
    static int weight;
    Panda( ):Herbivore(weight){

    }
    ~Panda(){
        //cout<<"Destroying Panda obj"<<endl;
    }
    string getAnimalType() override{
        return type;
    }
};


#endif //MYZOO_PANDA_H
