#include <iostream>
#include <memory>

#include "Food/Meet.h"
#include "Food/Grass.h"
#include "Food/Bambus.h"
#include "Animals/Tiger.h"
#include "Animals/Badger.h"
#include "Animals/Elephant.h"
#include "Animals/Panda.h"
#include "Zoo.h"
#include <ctime>
#include <vector>

using namespace std;


int Badger::weight = (srand(time(0)), 1 + (rand() % 16));
int Tiger::weight = (srand(time(0)), 1 + (rand() % 300));
int Elephant::weight =(srand(time(0)), 1 + (rand() % 2000));
int Panda::weight = (srand(time(0)), 1 + (rand() % 100));

int main() {

    Zoo *z=new Zoo();
    Tiger *tiger=new Tiger();
    Badger *badger=new Badger();
    Elephant *elephant=new Elephant();;
    Panda *panda=new Panda();

    cout << tiger->getAnimalType() << " " << tiger->getWeight() <<"kg"<< endl;
    cout << badger->getAnimalType() << " " << badger->getWeight() << endl;
    cout << elephant->getAnimalType() << " " << elephant->getWeight() << endl;
    cout<<panda->getAnimalType()<<" "<<panda->getWeight()<<endl;

    z->addAnimal(dynamic_cast<Animal*>(badger));
    z->addAnimal(dynamic_cast<Animal*>(tiger));
    z->addAnimal(dynamic_cast<Animal*>(elephant));
    z->addAnimal(dynamic_cast<Animal*>(panda));
    z->feedingTime();

    cout<<"\n"<<"Found haviest Herbivore: "<<z->getHaviestHerbivore()->getAnimalType()<<endl;
    cout<<"Found carnivore Herbivore: "<<z->getHaviestCarnivore()->getAnimalType()<<endl;

    z->deleteAnimal(1);
    z->deleteAnimal(2);
    z->clearZoo();
    delete(z);


}
