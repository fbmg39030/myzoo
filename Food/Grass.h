//
// Created by fbmg39030 on 26/03/21.
//

#ifndef MYZOO_GRASS_H
#define MYZOO_GRASS_H


#include "Plants.h"

class Grass: public Plants {
public:
    Grass():Plants(){

    }
    virtual ~Grass(){
        //cout<<"Destroying Grass obj"<<endl;
    }
    string getPlantType() override;
};


#endif //MYZOO_GRASS_H
