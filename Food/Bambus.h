//
// Created by fbmg39030 on 26/03/21.
//

#ifndef MYZOO_BAMBUS_H
#define MYZOO_BAMBUS_H


#include "Plants.h"

class Bambus: public Plants {
public:
    Bambus():Plants(){}
    virtual ~Bambus(){
        //cout<<"Destroying Bambus obj"<<endl;
    }
    string getPlantType() override;
};


#endif //MYZOO_BAMBUS_H
