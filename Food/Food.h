//
// Created by fbmg39030 on 26/03/21.
//

#ifndef MYZOO_FOOD_H
#define MYZOO_FOOD_H

#include <string>
#include <iostream>

using namespace std;

class Food {
public:
    virtual string getType();
    ~Food(){
        //cout<<"Destroying Food obj"<<endl<<endl;
    }
};


#endif //MYZOO_FOOD_H
