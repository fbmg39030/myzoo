//
// Created by fbmg39030 on 26/03/21.
//

#ifndef MYZOO_MEET_H
#define MYZOO_MEET_H

#include "string"
#include "Food.h"

using namespace std;

class Meet: public Food {
public:
    Meet()= default;
    virtual ~Meet(){
        //cout<<"Destroying Meet obj"<<endl;
    }
    string getType();
};


#endif //MYZOO_MEET_H
