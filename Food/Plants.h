//
// Created by fbmg39030 on 26/03/21.
//

#ifndef MYZOO_PLANTS_H
#define MYZOO_PLANTS_H


#include "Food.h"

class Plants: public Food {
public:
    Plants():Food(){

    }
    string getType() override{
        return "Plant";
    }
    virtual string getPlantType();
    ~Plants(){
        //cout<<"Destroying Plant obj"<<endl;
    }
};


#endif //MYZOO_PLANTS_H
