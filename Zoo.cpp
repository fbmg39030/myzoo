//
// Created by fbmg39030 on 28/03/21.
//

#include <thread>
#include "Zoo.h"
#include "Food/Meet.h"
#include "Food/Bambus.h"
#include "Food/Grass.h"
#include "Animals/Carnivore.h"
#include "Animals/Herbivore.h"

int Zoo::addAnimal(Animal *sensor) {
    vecAnimals.push_back(sensor);
    animalsID++;
    sensor->setId(animalsID);
    return animalsID;
}

void Zoo::clearZoo() {
    for (auto a: vecAnimals) {
        if (a != NULL) {
            delete (a);
        }
    }
}

Animal *Zoo::getAnimal(int id) {
    for (auto a: vecAnimals) {
        if (a->getId()==id){
            return a;
        }
    }
    return nullptr;
}

void Zoo::deleteAnimal(int id) {
    int count=0;
    for (auto a: vecAnimals) {
        if (a->getId()==id){
            vecAnimals.erase(vecAnimals.begin()+count);
        }
        count++;
    }
}

void Zoo::feedingTime() {
    //cout<<"\n";
    for (auto a: vecAnimals) {
        cout<<"\n";
        while (a->feeding(pickRandomFood())==1){
            cout<<"I'm picking another food for him"<<endl;
            this_thread::sleep_for(3s);
        }
    }
}

Food *Zoo::pickRandomFood() {
    srand(time(NULL));
    int randNum = (rand() % 3) + 1;
    if(randNum==1){
        return new Meet();
    }
    else if(randNum==2){
        return new Bambus();
    }
    else if(randNum==3){
        return new Grass();
    }

    return nullptr;
}

Animal *Zoo::getHaviestHerbivore() {
    Animal *haviestAnimal= nullptr;
    for (auto a: vecAnimals) {
        if (dynamic_cast<Herbivore *>(a)) {
            if(haviestAnimal== nullptr){
                haviestAnimal=a;
            }
            else if(a->getWeight()> haviestAnimal->getWeight()){
                haviestAnimal=a;
            }
        }
    }
    return haviestAnimal;
}

Animal *Zoo::getHaviestCarnivore() {
    Animal *haviestAnimal= nullptr;
    for (auto a: vecAnimals) {
        if (dynamic_cast<Carnivore *>(a)) {
            if(haviestAnimal== nullptr){
                haviestAnimal=a;
            }
            else if(a->getWeight()> haviestAnimal->getWeight()){
                haviestAnimal=a;
            }
        }
    }
    return haviestAnimal;
}

